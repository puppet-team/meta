This is the "meta" repository for the Debian Puppet Group.

# Checkout

To check out all puppet-team packages, do the following:

(I am assuming you want to keep your debian packaging in
"~/debian/puppet-team".  Change to taste)

Add the following line to ~/.mrtrust

    ~/debian/puppet-team/.mrconfig

Add the following stanza to ~/.mrconfig

    [debian/puppet-team]
    checkout = git clone 'git+ssh://salsa.debian.org/puppet-team/meta.git' 'puppet-team'
    chain = true

Running "mr up" will create a directory "~/debian/puppet-team/packages"
containing one git repository for each packagae, and keep your local
repositories up to date with the content on salsa.debian.org.

# New package

TODO: Make a script to do this.  This is a rough draft for package "mypackage".

1: Make a package repository locally

2: Make a package repository at salsa.debian.org

3: Push your changes

4: Add package repo stanza to puppet-team's .mrconfig with

  ./bin/make-mrconfig
  git add .mrconfig .gitignore
  git commit -m 'puppet-team: refresh mrconfig (new repo)'
  git push

# Update mrconfig

Create a file `salsarc` with your Gitlab token.

```
SALSA_TOKEN="your-token"
```

Create or update the project list and update the mrconfig file

```
./bin/make-projects-list
./bin/make-mrconfig
```
