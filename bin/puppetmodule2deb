#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 Thomas Bechtold <thomasbechtold@jpberlin.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import argparse
import os
import shutil
import subprocess
import json
from email.utils import formatdate
import re
import datetime

# use this template for debian/copyright if license seems to be apache 2.0
APACHE_2_LICENSE = \
"""License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License 2.0 can
 be found in "/usr/share/common-licenses/Apache-2.0"
"""

class PuppetModule(object):
    """class to generate a debian package from a puppet module"""
    def __init__(self, upstream_source, maintainer, dest_dir):
        self.__maintainer = maintainer
        #currently only .tar.gz files are supported
        if not upstream_source.endswith(".tar.gz"):
            raise ValueError("only .tar.gz files are supported.")
        #check that upstream source exists
        self.__upstream_source = os.path.abspath(upstream_source)
        if not os.path.exists(self.__upstream_source):
            raise ValueError("upstream source {0} does not exist"
                             .format(self.__upstream_source))
        #check that dest dir does not exists
        self.__dest_dir = os.path.abspath(dest_dir)
        if os.path.exists(self.__dest_dir):
            raise ValueError("destination directory {0} already exists".
                             format(self.__dest_dir))
        else:
            os.mkdir(self.__dest_dir)

        #copy module to dest dir
        upstream_source_dest = os.path.join(self.__dest_dir, "puppet-module-{0}".format(os.path.basename(self.__upstream_source)))
        shutil.copy(self.__upstream_source, upstream_source_dest)
        self.__upstream_source = upstream_source_dest

        #start processing the information
        self.__get_maintainer()
        self.__parse_upstream_source_name()
        self.__create_pkg_git_dir()
        self.__git_init()
        self.__git_import_orig()
        self.__parse_metadata()
        os.mkdir("debian")
        self.__debian_source()
        self.__debian_compat()
        self.__debian_rules()
        self.__debian_install()
        self.__debian_postinst()
        self.__debian_prerm()
        self.__debian_readme()
        self.__debian_control()
        self.__debian_copyright()
        self.__debian_changelog()
        self.__debian_docs()
        self.__debian_watch()
        self.__git_add_debian()

    def __get_maintainer(self):
        if not self.__maintainer:
            fullname = os.environ.get('DEBFULLNAME')
            if not fullname:
                raise ValueError("no maintainer given and $DEBFULLNAME not set")
            email = os.environ.get('DEBEMAIL')
            if not email:
                raise ValueError("no maintainer given and $DEBEMAIL not set")
            self.__maintainer = "{0} <{1}>".format(fullname, email)

    def __parse_upstream_source_name(self):
        """get  module user (ie puppetlabs), module namespace (ie stdlib)
        and module name (ie puppet-module-puppetlabs-stdlib"""
        name = os.path.basename(self.__upstream_source.rstrip(".tar.gz"))
        _waste, _waste, self.__module_user, self.__module_namespace, self.__module_version = name.split("-")
        self.__module_name = "puppet-module-{0}-{1}".format(self.__module_user, self.__module_namespace)

    def __create_pkg_git_dir(self):
        """create directory for package (ie puppet-module-puppetlabs-stdlib"""
        self.__module_git_dir = os.path.join(self.__dest_dir, "{0}".format(self.__module_name))
        os.mkdir(self.__module_git_dir)

    def __git_init(self):
        os.chdir(self.__module_git_dir)
        subprocess.check_call("git init", shell=True)

    def __git_import_orig(self):
        subprocess.check_call("gbp import-orig --pristine-tar --no-interactive {0}".format(self.__upstream_source), shell=True)

    def __git_add_debian(self):
        subprocess.check_call("git add debian/", shell=True)
        subprocess.check_call('git commit -m "Add debian/ dir"', shell=True)

    def __parse_metadata(self):
        metadata_path = os.path.join(self.__module_git_dir, "metadata.json")
        if not os.path.exists(metadata_path):
            raise ValueError("metadata.json not found in module. can not extract dependencies")

        with open(metadata_path, "r") as f:
            self.__module_metadata = json.load(f)
            #check that we have the same version from filename and from metadata
            if self.__module_metadata['version'] != self.__module_version:
                raise ValueError("module version from filename ({0}) "
                                 "and metadata.json ({1}) not equal"
                                 .format(self.__module_version, self.__module_metadata['version']))

    def __debian_source(self):
        os.mkdir(os.path.join("debian", "source"))
        with open(os.path.join("debian", "source", "format"), "w") as f:
            f.write("3.0 (quilt)")

    def __debian_compat(self):
        with open(os.path.join("debian", "compat"), "w") as f:
            f.write("11")

    def __debian_install(self):
        with open(os.path.join("debian", "{0}.install".format(self.__module_name)), "w") as f:
            #iterate over possible files and subdirs
            for d in ["Modulefile", "metadata.json", "files", "templates", "tests", "manifests", "lib"]:
                if os.path.exists(d):
                    f.write("{0} usr/share/puppet/modules.available/{1}-{2}\n".format(d, self.__module_user, self.__module_namespace))

    def __debian_rules(self):
        with open(os.path.join("debian", "rules"), "w") as f:
            f.write(
"""#!/usr/bin/make -f
%:
\tdh $@

""")
        subprocess.check_call("chmod a+x {0}".format(os.path.join("debian", "rules")), shell=True)

    def __debian_postinst(self):
        with open(os.path.join("debian", "{0}.postinst".format(self.__module_name)), "w") as f:
            f.write(
"""#!/bin/sh
#
# see: dh_installdeb(1)

set -e

# summary of how this script can be called:
#        * <postinst> `configure' <most-recently-configured-version>
#        * <old-postinst> `abort-upgrade' <new version>
#        * <conflictor's-postinst> `abort-remove' `in-favour' <package>
#          <new-version>
#        * <postinst> `abort-remove'
#        * <deconfigured's-postinst> `abort-deconfigure' `in-favour'
#          <failed-install-package> <version> `removing'
#          <conflicting-package> <version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package

case "$1" in
    configure)
        update-alternatives --install /usr/share/puppet/modules/{1} puppet-module-{1} /usr/share/puppet/modules.available/{0}-{1} 500
        ;;

    abort-upgrade|abort-remove|abort-deconfigure)
        ;;

    *)
        echo "postinst called with unknown argument \`$1'" >&2
        exit 1
        ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
""".format(self.__module_user, self.__module_namespace))

    def __debian_prerm(self):
        with open(os.path.join("debian", "{0}.prerm".format(self.__module_name)), "w") as f:
            f.write(
"""#!/bin/sh
#
# see: dh_installdeb(1)

set -e

# summary of how this script can be called:
#        * <prerm> `remove'
#        * <old-prerm> `upgrade' <new-version>
#        * <new-prerm> `failed-upgrade' <old-version>
#        * <conflictor's-prerm> `remove' `in-favour' <package> <new-version>
#        * <deconfigured's-prerm> `deconfigure' `in-favour'
#          <package-being-installed> <version> `removing'
#          <conflicting-package> <version>
# for details, see http://www.debian.org/doc/debian-policy/ or
# the debian-policy package


case "$1" in
    remove|upgrade|deconfigure)
        update-alternatives --remove puppet-module-{1} /usr/share/puppet/modules.available/{0}-{1}
    ;;

    failed-upgrade)
    ;;

    *)
        echo "prerm called with unknown argument \`$1'" >&2
        exit 1
    ;;
esac

# dh_installdeb will replace this with shell code automatically
# generated by other debhelper scripts.

#DEBHELPER#

exit 0
""".format(self.__module_user, self.__module_namespace))

    def __debian_readme(self):
        with open(os.path.join("debian", "README.Debian"), "w") as f:
            f.write(
"""This package contains a module for use by puppet. It is
automatically added to the module load path of puppet's default
environment, and should be ready for use.

You can use "update-alternatives --config puppet-module-{0}"
to configure which module should appear in the module path of the
default puppet environment, in case multiple modules which provide
the same name are installed.
""".format(self.__module_namespace))

    def __debian_control_depends(self):
        """extract dependencies from metadata and try to create debian binary deps"""
        depends = []
        for dep in self.__module_metadata['dependencies']:
            dep_user, dep_module = dep['name'].split('/')
            dep_package_name = "puppet-module-{0}-{1}".format(dep_user, dep_module)
            dep_versions = re.findall("\s*[<>=]*\s*[0-9]\.[0-9]\.[0-9]", dep['version_requirement'].rstrip().lstrip())
            for v in dep_versions:
                v = v.strip()
                v = re.sub("\s*(?P<sign>[<,>])\s*(?P<vstart>[0-9])", "\g<sign>= \g<vstart>", v)
                depends.append("{0} ({1})".format(dep_package_name, v))
        return depends

    def __debian_control(self):
        """
        TODO: add VCS-Fields
        """
        deps = self.__debian_control_depends()
        deps_str = ""
        if deps:
            deps_str += ",\n         "
            deps_str += ",\n         ".join(deps)
            deps_str +=","
        with open(os.path.join("debian", "control"), "w") as f:
            f.write(
"""Source: {0}
Section: admin
Priority: optional
Maintainer: Puppet Package Maintainers <pkg-puppet-devel@lists.alioth.debian.org>
Uploaders: {3}
Build-Depends: debhelper (>= 11)
Standards-Version: 4.1.3
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/puppet-team/{0}.git
Vcs-Browser: https://salsa.debian.org/puppet-team/{0}
Homepage: https://forge.puppetlabs.com/{1}/{2}

Package: {0}
Architecture: all
Depends: ${{misc:Depends}},
         puppet (>= 4) | puppet-common (>= 3){4}
Description: Puppet module for {2}
 Puppet lets you centrally manage every important aspect of your system
 using a cross-platform specification language that manages all the
 separate elements normally aggregated in different files, like users,
 cron jobs, and hosts, along with obviously discrete elements like
 packages, services, and files.
 .
 FIXME: Describe here the usecase for the given module!
""".format(self.__module_name, self.__module_user, self.__module_namespace, self.__maintainer, deps_str))

    def __debian_copyright(self):
        #try to guess the used license
        license_type = "License: FIXME"
        if os.path.exists("LICENSE"):
            with open("LICENSE", "r") as l:
                    if "http://www.apache.org/licenses/LICENSE-2.0" in l.read():
                        license_type = APACHE_2_LICENSE
        with open(os.path.join("debian", "copyright"), "w") as f:
            f.write(
"""Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: {0}-{1}
Source: https://forge.puppetlabs.com/{0}/{1}

Files: *
Copyright: FIXME
License: FIXME

Files: debian/*
Copyright: {2} {3}
License: FIXME (hint: use same license as module)

{4}

""".format(self.__module_user, self.__module_namespace, datetime.datetime.now().year, self.__maintainer, license_type))

    def __repr__(self):
        return "{0} ({1})".format(self.__module_name, self.__module_version)

    def __debian_changelog(self):
        with open(os.path.join("debian", "changelog"), "w") as f:
            f.write(
"""{0} ({1}-1) UNRELEASED; urgency=low

  * Initial release (Closes: #FIXME)

 -- {2}  {3}
""".format(self.__module_name, self.__module_version, self.__maintainer,
           formatdate()))

    def __debian_docs(self):
        """try to find documentation files"""
        with open(os.path.join("debian", "{0}.docs".format(self.__module_name)), "w") as f:
            for d in ["README.md", "README.markdown", "CONTRIBUTION.md", "NOTICE"]:
                if os.path.exists(d):
                    f.write("{0}\n".format(d))

    def __debian_watch(self):
        with open(os.path.join("debian", "watch"), "w") as f:
            f.write(
"""version=4
https://qa.debian.org/cgi-bin/fakeupstream.cgi?upstream=forge.puppetlabs/{0}/{1} .*/{0}-{1}-(.+)\.tar\.gz
""".format(self.__module_user, self.__module_namespace))

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("upstream_source",
                        help="Path to the upstream tarball")
    parser.add_argument("--maintainer", help='ie "Thomas Bechtold <thomasbechtold@jpberlin.de>". If not given $DEBFULLNAME and $DEBEMAIL are used.')
    parser.add_argument("--dest", help="destination directory for packaging. directory must not exist. default is ./dest", default="./dest")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    mod = PuppetModule(args.upstream_source, args.maintainer, args.dest)
