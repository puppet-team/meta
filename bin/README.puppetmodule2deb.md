# puppetmodule2deb

Convert puppet modules from http://forge.puppetlabs.com to debian packages.
This little program uses git-buildpackage to create a Debian package.
Install the dependencies first:

        apt-get install git-buildpackage pristine-tar

## Converting a module

1. Download a module from forge.puppetlabs.com

	wget http://forge.puppetlabs.com/puppetlabs/apt/1.2.0.tar.gz

2. Run puppetmodule2deb:

	./puppetmodule2deb puppetlabs-apt-1.2.0.tar.gz --maintainer="Me <me@example.com>"

3. Try to build the package:

	cd dest/pkg-puppet-module-puppetlabs-apt
	git-buildpackage -us -uc

4. There are some `FIXME` marks in the created debian/ files. Not everything can be automated.
Also the dependencies are currently not perfect. But the package should work and should be installable
if all the dependencies are available.

## Tips & Tricks

1. Grep for `FIXME` after you created a Debian package with puppetmodule2deb.
2. Check help with

	puppetmodule2deb -h


